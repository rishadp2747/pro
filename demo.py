from cluster_methods.missingness_cluster import miscluster
from cluster_methods.projection_cluster import projcluster
from model.model import gsrs
from tools.tools import *
from sys import path
import pandas as pd
import numpy as np
import time

data = pd.read_csv(r'data/movielens_data.csv', header=None)
# ucold, icold = projcluster(data)

data = data.sample(frac=1).reset_index(drop=True)
# print(data.first)
split_index = int(len(data) * 0.8)
train = data[:split_index].reset_index(drop=True)
test = data[split_index:].reset_index(drop=True)

ug,ig = miscluster(train, 10)
print(ug)
clusters = {}
clusters['uclusters'] = ug
clusters['iclusters'] = ig

data2 = pd.read_csv(r'data/movielens_data_inc.csv', header=None)
data2 = data2.sample(frac=1).reset_index(drop=True)

ug2, ig2 = miscluster(data2, 10)
clusters2 = {'uclusters': ug2, 'iclusters': ig}

paras = {'lambda':10, 'K':300, 'nstep':50, 'stopall':1e-3, 'stopone':1e-5, 'verbose':True, 'bias': False}
test1 = gsrs(paras)
test1.train(train[[0,1]], train[2], clusters)
result = np.array(test1.predict(test[[0,1]]))

test1.increase(data2[[0, 1]], data2[2], clusters2)

print(test1.icluster)
# print(test1.data.head())
# ucold1, icold1 = projcluster(train)
# print("hi")
# ucold2, icold2 = projcluster(test)
# ucold, icold = projcluster(data)

# result_1 = np.array(test)
print("Ucold and Icold")
# print(str(ucold1), str(icold1))
#   print(str(ucold), str(icold))
print(str(rmse_evaluate(test[2], result)))
